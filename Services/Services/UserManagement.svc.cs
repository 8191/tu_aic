﻿using Library;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Runtime.Serialization;
using System.Security.Cryptography;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using System.Data;

namespace Services.UserMgmt
{
    [ServiceBehavior(Namespace = "http://aic.conf.at/")]
    public class UserManagementService : UserManagementInterface
    {
        private DatabaseEntities databaseContext;

        public UserManagementService()
        {
            databaseContext = new DatabaseEntities();
        }

        public bool Register(string realName, string username, string password, string emailAddress, string company)
        {
            User user = new User();
            user.username = username;
            user.realname = realName;
            user.password = HashPassword(username, password);
            user.emailaddress = emailAddress;
            user.registration = DateTime.Now;
            user.company = company;

            databaseContext.users.Add(user);

            try
            {
                int res = databaseContext.SaveChanges();

                return res > 0;
            }
            catch (DataException)
            {
                return false;
            }
        }

        public bool UserExists(string username)
        {
            User user =
                (from u in databaseContext.users
                 where u.username == username
                 select u).FirstOrDefault();

            return user != null;
        }

        public bool Unregister(string username)
        {
            // Delete opened sessions
            IQueryable<Session> userSessions =
                from s in databaseContext.sessions
                where s.username == username
                select s;
            foreach (Session session in userSessions)
                databaseContext.sessions.Remove(session);

            // Delete user
            User user =
                (from u in databaseContext.users
                 where u.username == username
                 select u).FirstOrDefault();
            if (user != null)
                databaseContext.users.Remove(user);
            int res = databaseContext.SaveChanges();

            return res > 0;
        }

        public string Login(string username, string password)
        {
            // Check if user exists and password is correct
            User user =
                (from u in databaseContext.users
                 where u.username == username
                 select u).FirstOrDefault();
            if (user == null || HashPassword(username, password) != user.password)
            {
                return null;
            }

            string sid = GenerateSessionId(user);

            // Delete old sessions
            IQueryable<Session> userSessions =
                from s in databaseContext.sessions
                where s.username == user.username
                select s;
            foreach (Session s in userSessions)
                databaseContext.sessions.Remove(s);

            // Add session
            Session session = new Session();
            session.sessionId = sid;
            session.user = user;
            session.time = DateTime.Now;
            databaseContext.sessions.Add(session);

            databaseContext.SaveChanges();

            return sid;
        }

        public bool IsLoggedIn(string sid)
        {
            // Query session
            Session session =
                (from s in databaseContext.sessions
                 where s.sessionId == sid
                 select s).FirstOrDefault();

            // Check if session exists and is valid
            if (session == null || (session.time.GetValueOrDefault() - DateTime.Now).Seconds > Properties.Settings.Default.LoginTime)
            {
                return false;
            }

            // Update session time
            session.time = DateTime.Now;
            databaseContext.SaveChanges();

            return true;
        }

        public string GetUsername(string sid)
        {
            // Query session
            Session session =
                (from s in databaseContext.sessions
                 where s.sessionId == sid
                 select s).FirstOrDefault();

            // Check if session exists and is valid
            if (session == null || (session.time.GetValueOrDefault() - DateTime.Now).Seconds > Properties.Settings.Default.LoginTime)
            {
                return null;
            }

            return session.username;
        }

        private static string HashPassword(string username, string password)
        {
            byte[] bytes = Encoding.Unicode.GetBytes(username + password + Properties.Settings.Default.PasswordSalt);
            byte[] inArray = HashAlgorithm.Create("SHA1").ComputeHash(bytes);
            return Convert.ToBase64String(inArray);
        }

        private static string GenerateSessionId(User user)
        {
            byte[] bytes = Encoding.Unicode.GetBytes(DateTime.Now.ToString() + user.password + Properties.Settings.Default.SessionIdSalt);
            byte[] inArray = HashAlgorithm.Create("SHA1").ComputeHash(bytes);
            return Convert.ToBase64String(inArray);
        }

        ~UserManagementService()
        {
            databaseContext.Dispose();
        }
    }
}
