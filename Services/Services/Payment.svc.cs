﻿using Library;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace Services.Payment
{
    [ServiceBehavior(Namespace = "http://aic.conf.at/")]
    public class PaymentService : PaymentInterface
    {
        private DatabaseEntities databaseContext;
        private UserManagementInterface userManagementService;

        public PaymentService()
        {
            // Connect to user management service
            BasicHttpBinding basicHttpBinding = new BasicHttpBinding();
            EndpointAddress endpointAddress = new EndpointAddress(Properties.Settings.Default.UserManagementServiceUrl);
            userManagementService = new ChannelFactory<UserManagementInterface>(basicHttpBinding, endpointAddress).CreateChannel();

            // Connect to database
            databaseContext = new DatabaseEntities();
        }

        public Bill GetBill(string uid)
        {
            // Check if user is logged in and get user object
            User user = GetLoggedInUser(uid);
            if (user == null)
            {
                return null;
            }

            DateTime lastBilling = (user.lastpaid == null) ? user.registration.GetValueOrDefault() : user.lastpaid.GetValueOrDefault();

            Bill bill = new Bill();
            bill.BillPeriod = DateTime.Now - lastBilling;
            bill.DailyFee = Properties.Settings.Default.DailyFee;
            bill.QueryFee = Properties.Settings.Default.QueryFee;
            bill.Queries = (user.unpaidrequests == null) ? 0 : user.unpaidrequests.GetValueOrDefault();
            bill.Sum = bill.BillPeriod.Days * bill.DailyFee + bill.Queries * bill.QueryFee;

            return bill;
        }

        public bool PayBill(string uid, PaymentInformation information)
        {
            // Check if user is logged in and get user object
            User user = GetLoggedInUser(uid);
            Bill bill = GetBill(uid);
            if (user == null || bill == null || information == null)
            {
                return false;
            }

            // Check if paid amount is sufficient
            if (information.Amount < bill.Sum)
            {
                return false;
            }

            user.lastpaid = DateTime.Now;
            user.unpaidrequests = 0;

            return true;
        }

        public bool AddQueryToBill(string uid, int queries)
        {
            // Check if user is logged in and get user object
            User user = GetLoggedInUser(uid);
            if (user == null)
            {
                return false;
            }

            // Increment unpaid requests
            if (user.unpaidrequests == null)
                user.unpaidrequests = 1;
            else
                user.unpaidrequests++;

            int res = databaseContext.SaveChanges();
            return res > 0;
        }

        private User GetLoggedInUser(string uid)
        {
            if (!userManagementService.IsLoggedIn(uid))
            {
                return null;
            }

            string username = userManagementService.GetUsername(uid);
            User user =
                (from u in databaseContext.users
                 where u.username == username
                 select u).FirstOrDefault();
            if (user == null)
            {
                return null;
            }

            return user;
        }

        ~PaymentService()
        {
            databaseContext.Dispose();
        }
    }
}
