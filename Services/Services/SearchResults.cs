﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace Services.Query
{
    [DataContract]
    public class SearchResults
    {
        public  SearchResults()
        {
                Results = new List<SearchResult>();
        }
 
        [DataMember(Name = "results")]
        public List<SearchResult> Results { get; set; }
    }
}
