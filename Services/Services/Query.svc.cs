﻿using Library;

using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Web;
using System.Net;
using System.IO;
using System.Runtime.Serialization.Json;
using System.Collections.Specialized;

namespace Services.Query
{
    [ServiceBehavior(Namespace = "http://aic.conf.at/")]
    public class QueryService : QueryInterface
    {
        private UserManagementInterface userManagementService;
        private PaymentInterface paymentService;

        public QueryService()
        {
            // connect to user management service
            BasicHttpBinding basicHttpBinding = new BasicHttpBinding();
            EndpointAddress userManagementEndpointAddress = new EndpointAddress(Properties.Settings.Default.UserManagementServiceUrl);
            userManagementService = new ChannelFactory<UserManagementInterface>(basicHttpBinding, userManagementEndpointAddress).CreateChannel();

            EndpointAddress paymentEndpointAddress = new EndpointAddress(Properties.Settings.Default.PaymentServiceUrl);
            paymentService = new ChannelFactory<PaymentInterface>(new BasicHttpBinding(), paymentEndpointAddress).CreateChannel();
        }

        public QueryResult Query(string uid, string searchTerm, DateTime beginTime, DateTime endTime)
        {
            // Check if user is logged in
            if (!userManagementService.IsLoggedIn(uid))
            {
                return null;
            }

            // Add query to bill
            bool status = paymentService.AddQueryToBill(uid, 1);
            if (!status)
            {
                return null;
            }

            decimal negative = 0;
            decimal positive = 0;
            List<Tweet> tweets = RequestTweets(searchTerm, beginTime, endTime);

            foreach (Tweet tweet in tweets)
            {
                decimal sentiment = AnalyzeTweet(tweet);

                if (sentiment == 1)
                    negative = negative + 1;
                else
                    positive = positive + 1;
            }

            QueryResult result = new QueryResult();
            result.BeginTime = beginTime;
            result.EndTime = endTime;
            result.SearchTerm = searchTerm;
            result.Negative = negative;
            result.Positive = positive;
            // Return percentage of positive Tweets (or 0 if no Tweets were found)
            result.Sentiment = (negative + positive > 0) ? positive / (negative + positive) : 0;

            return result;
        }

        private decimal AnalyzeTweet(Tweet tweet)
        {
            string parameter = "";
            NameValueCollection parameters = new NameValueCollection() {
                {"format", "html"},
                {"apikey", Properties.Settings.Default.TextClassifyApiKey},
                {"classifierid", "27"},
                {"text", tweet.Text}
            };

            foreach (string key in parameters)
                parameter += String.Format("{0}={1}&",
                    HttpUtility.UrlEncode(key),
                    HttpUtility.UrlEncode(parameters[key]));

            HttpWebRequest myHttpWebRequest = (HttpWebRequest)HttpWebRequest.Create(Properties.Settings.Default.TextClassifyServiceUrl);
            myHttpWebRequest.Method = "POST";

            byte[] data = Encoding.ASCII.GetBytes(parameter);

            myHttpWebRequest.ContentType = "application/x-www-form-urlencoded";
            myHttpWebRequest.ContentLength = data.Length;

            Stream requestStream = myHttpWebRequest.GetRequestStream();
            requestStream.Write(data, 0, data.Length);
            requestStream.Close();

            HttpWebResponse myHttpWebResponse = (HttpWebResponse)myHttpWebRequest.GetResponse();

            Stream responseStream = myHttpWebResponse.GetResponseStream();

            StreamReader myStreamReader = new StreamReader(responseStream, Encoding.Default);

            string pageContent = myStreamReader.ReadToEnd();

            myStreamReader.Close();
            responseStream.Close();
            myHttpWebResponse.Close();

            return pageContent.Contains("negative") ? 1 : 0;
        }

        private string PerformRequest(string method, string url)
        {
            HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(url);
            request.Method = method;

            WebResponse response = request.GetResponse();
            StreamReader reader = new StreamReader(response.GetResponseStream());

            string responseString = reader.ReadToEnd();

            reader.Close();

            return responseString;
        }

        private string Post(string url)
        {
            return PerformRequest("POST", url);
        }

        private string Get(string url)
        {
            return PerformRequest("GET", url);
        }

        public List<Tweet> RequestTweets(string searchText, DateTime beginTime, DateTime endTime)
        {
            string Byear = beginTime.Year.ToString();
            string Bmonth = beginTime.Month.ToString("D2");
            string Bday = beginTime.Day.ToString("D2");

            string Eyear = endTime.Year.ToString();
            string Emonth = endTime.Month.ToString("D2");
            string Eday = endTime.Day.ToString("D2");

            //  +since%3A2011-06-20+until%3A2011-06-20
            string datepart = "+since%3A" + Byear + "-" + Bmonth + "-" + Bday + "+until%3A" + Eyear + "-" + Emonth + "-" + Eday;

            string url = string.Format(Properties.Settings.Default.TwitterApiUrl + "?q=" + searchText + datepart);

            string response = Get(url);

            byte[] brr = ASCIIEncoding.UTF8.GetBytes(response);
            StreamReader reader = new StreamReader(new MemoryStream(brr));
            DataContractJsonSerializer serializer =
              new DataContractJsonSerializer(typeof(SearchResults));
            SearchResults searchResults =
              (SearchResults)serializer.ReadObject(reader.BaseStream);
            reader.Close();

            List<Tweet> tweets =

              (from u in searchResults.Results
               select new Tweet
               {
                   Text = HttpUtility.HtmlDecode(u.Text),
                   Date = DateTimeOffset.Parse(u.CreatedAt).UtcDateTime
               }).ToList();

            return tweets;
        }

    }
}
