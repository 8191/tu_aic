﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Services.Query
{
    public class Tweet
    {
        public string Text { get; set; }
        public DateTimeOffset Date { get; set; }
    }
}
