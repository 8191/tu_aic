﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ServiceModel;

namespace Library
{
    [ServiceContract(Namespace = "http://aic.conf.at/")]
    public interface QueryInterface
    {
        [OperationContract]
        QueryResult Query(string uid, string searchTerm, DateTime beginTime, DateTime endTime);
    }
}
