﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ServiceModel;
using System.Runtime.Serialization;

namespace Library
{
    [DataContract]
    public class PaymentInformation
    {
        [DataMember]
        public string Method { get; set; }

        [DataMember]
        public string Information { get; set; }

        [DataMember]
        public double Amount { get; set; }
    }
}
