﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ServiceModel;
using System.Runtime.Serialization;

namespace Library
{
    [DataContract]
    public class QueryResult
    {
        [DataMember]
        public DateTime BeginTime { get; set; }

        [DataMember]
        public DateTime EndTime { get; set; }

        [DataMember]
        public string SearchTerm { get; set; }

        [DataMember]
        public decimal Negative { get; set; }

        [DataMember]
        public decimal Positive { get; set; }

        [DataMember]
        public int Count { get; set; }

        [DataMember]
        public decimal Sentiment { get; set; }
    }
}
