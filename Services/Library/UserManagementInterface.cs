﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ServiceModel;

namespace Library
{
    [ServiceContract(Namespace = "http://aic.conf.at/")]
    public interface UserManagementInterface
    {
        [OperationContract]
        bool Register(string realName, string username, string password, string emailAddress, string company);

        [OperationContract]
        bool UserExists(string username);

        [OperationContract]
        bool Unregister(string username);

        [OperationContract]
        string Login(string username, string password);

        [OperationContract]
        bool IsLoggedIn(string sid);

        [OperationContract]
        string GetUsername(string sid);
    }
}
