﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ServiceModel;
using System.Runtime.Serialization;

namespace Library
{
    [DataContract]
    public class Bill
    {
        [DataMember]
        public TimeSpan BillPeriod { get; set; }
        
        [DataMember]
        public int Queries { get; set; }

        [DataMember]
        public double DailyFee { get; set; }

        [DataMember]
        public double QueryFee { get; set; }

        [DataMember]
        public double Sum { get; set; }
    }
}
