﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ServiceModel;

namespace Library
{
    [ServiceContract(Namespace = "http://aic.conf.at/")]
    public interface PaymentInterface
    {
        [OperationContract]
        Bill GetBill(string uid);

        [OperationContract]
        bool PayBill(string uid, PaymentInformation information);

        [OperationContract]
        bool AddQueryToBill(string uid, int queries);
    }
}
