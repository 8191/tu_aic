USE [aic]
GO

/****** Object:  Table [dbo].[sessions]    Script Date: 20-Dec-12 11:17:53 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[sessions](
	[sessionId] [varchar](50) NOT NULL,
	[username] [varchar](50) NULL,
	[time] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[sessionId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[sessions]  WITH CHECK ADD  CONSTRAINT [FK_sessions_users] FOREIGN KEY([username])
REFERENCES [dbo].[users] ([username])
GO

ALTER TABLE [dbo].[sessions] CHECK CONSTRAINT [FK_sessions_users]
GO

