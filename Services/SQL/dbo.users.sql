USE [aic]
GO

/****** Object:  Table [dbo].[users]    Script Date: 20-Dec-12 11:17:11 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[users](
	[username] [varchar](50) NOT NULL,
	[realname] [varchar](max) NULL,
	[password] [varchar](50) NOT NULL,
	[emailaddress] [varchar](50) NOT NULL,
	[company] [varchar](max) NULL,
	[registration] [date] NULL,
	[unpaidrequests] [int] NULL,
	[lastpaid] [date] NULL,
PRIMARY KEY CLUSTERED 
(
	[username] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

